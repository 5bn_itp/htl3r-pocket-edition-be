# HTL3R Pocket Edition BE

## Setup
### Set config file
The config file must be placed in the top level directory and must be named ``config.toml``
Example:
```toml
[server]
host = "127.0.0.1"
port = 8080

[azure_client]
client_id = "<CLIENT_ID>"
client_secret = "<CLIENT_SECRET>"
authority = "<AUTHORITY>"
redirect_path = "<REDIRECT_PATH>"
scope = ["<SCOPE_A>", "<SCOPE_B>", ...]

[api]
specification_dir="api-definitions/"
openapi_file="backend_openapi.yaml"

[sqlalchemy]
echo=true

# settings this will create a database in this location when executing the create_db script
database_uri="sqlite:///htl3r_pe_db/sqlite.db"
track_modifications=false
```

### Create Database
1. Execute ``.\scripts\create_db.bat`` or ``.\scripts\create_db.sh``
2. Now under ``./htl3r_pe_db`` there should be a `.db` file; the name should be equal to what is set in the config file