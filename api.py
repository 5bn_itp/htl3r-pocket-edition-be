from htl3r_pe_db.helpers import *
from htl3r_pe_db.models import *
from msal_adapter import msal_client
from azure_ad_verify_token import verify_jwt
from config import config


def verify_token(token):
    try:
        payload = verify_jwt(
            token=token,
            valid_audiences=[config.azure_client.client_id],
            issuer=f'{config.azure_client.authority}/v2.0',
            jwks_uri=f'{config.azure_client.authority}/discovery/v2.0/keys',
            verify=True
        )

        if not user_exists(payload["oid"]):
            User.Schema.create(User(
                user_oid=payload["oid"],
                role_id=1,
                user_name=payload["name"],
                user_email=payload["preferred_username"]
            ))

        return payload
    except Exception as e:
        print(e)
        return None


# unused
def login_url(body, **kwargs):
    return msal_client.initiate_auth_code_flow([], redirect_uri=body["redirect_uri"])


# unused
def logout_url(body, **kwargs):
    return {
        'redirect_uri': f'{config.azure_client.authority}/oauth2/v2.0/logout?post_logout_redirect_uri={body["redirect_uri"]}'
    }


def getAllFAQs(**kwargs):
    return FAQ.Schema.fetchAll()


def addFAQ(body, token_info, **kwargs):
    request_id = add_request_for(token_info)
    return FAQ.Schema.create(FAQ(
        request_id=request_id,
        answer=body["answer"],
        question=body["question"]
    ))


def getFAQ(faq_id, **kwargs):
    return FAQ.Schema.fetch(faq_id)


def updateFAQ(faq_id, body, **kwargs):
    return FAQ.Schema.update(faq_id, body)


@is_authorized(allowed_roles=[2, 3])
def deleteFAQ(faq_id, **kwargs):
    return FAQ.Schema.delete(faq_id)


@is_authorized(allowed_roles=[2, 3])
def getAllModeratorRequests(**kwargs):
    return Request.Schema.fetchAll()


@is_authorized(allowed_roles=[2, 3])
def addModeratorRequest(body, **kwargs):
    return Request.Schema.create(Request(
        requested_by=body["requested_by"]
    ))


def getModeratorRequest(request_id, **kwargs):
    return Request.Schema.fetch(request_id)


@is_authorized(allowed_roles=[2, 3])
def updateModeratorRequest(request_id, body, **kwargs):
    return Request.Schema.update(request_id, body)


@is_authorized(allowed_roles=[2, 3])
def deleteModeratorRequest(request_id, **kwargs):
    return Request.Schema.delete(request_id)


def getAllGuides(**kwargs):
    return Guide.Schema.fetchAll()


def addGuide(body, token_info, **kwargs):
    request_id = add_request_for(token_info)
    return Guide.Schema.create(Guide(
        request_id=request_id,
        topic=body["topic"],
        content=body["content"]
    ))


def getGuide(guide_id, **kwargs):
    return Guide.Schema.fetch(guide_id)


@is_authorized(allowed_roles=[2, 3])
def updateGuide(guide_id, body, **kwargs):
    return Guide.Schema.update(guide_id, body)


@is_authorized(allowed_roles=[2, 3])
def deleteGuide(guide_id, **kwargs):
    return Guide.Schema.delete(guide_id)


def getAllProjects(**kwargs):
    return ProjectTeamSchema(many=True).dump(Project.query.all())


def addProject(body, token_info, **kwargs):
    request_id = add_request_for(token_info)
    project = Project.Schema.create(Project(
        request_id=request_id,
        topic=body["topic"],
        state_id=body["status"],
        title=body["title"],
        short_description=body["short_description"],
        logo=body["logo"],
        team_image=body["team_image"],
        project_image=body["project_image"],
        long_description=body["long_description"],
        other_information=body["other_information"]
    ))

    team = body["team"]
    for member in team:
        TeamMember.Schema.create(TeamMember(
            project_id=project["project_id"],
            user_id=member
        ))
    return project


def getProject(project_id, **kwargs):
    return ProjectTeamSchema().dump(Project.query.get(project_id))


def updateProject(project_id, body, token_info, **kwargs):
    if not _is_team_member_or_moderator_or_admin(project_id, token_info["oid"]):
        return _unauthorized()

    if "team" in body:
        team = body["team"]
        TeamMember.query \
            .filter_by(project_id=project_id) \
            .delete()
        for member in team:
            TeamMember.Schema.create(TeamMember(project_id=project_id, user_id=member))

    del body["team"]
    Project.Schema.update(project_id, body)

    return ProjectTeamSchema().dump(Project.query.get(project_id))


@is_authorized(allowed_roles=[2, 3])
def deleteProject(project_id, **kwargs):
    TeamMember.query \
        .filter_by(project_id=project_id) \
        .delete()
    return Project.Schema.delete(project_id)


def getAllUsers(**kwargs):
    return User.Schema.fetchAll()


@is_authorized(allowed_roles=[3])
def addUser(body, token_info, **kwargs):
    oid = token_info["oid"]
    user_name = token_info["name"]
    user_email = token_info["preferred_username"]
    return User.Schema.create(User(
        oid=oid,
        user_name=user_name,
        user_email=user_email,
        user_role_id=body["user_role_id"]
    ))


def getUser(user_id, **kwargs):
    return User.Schema.fetch(user_id)


def getUserByOid(user_oid, **kwargs):
    user = User.query.filter_by(user_oid=user_oid).first()
    return User.Schema().dump(user)


def getCurrentUser(token_info, **kwargs):
    oid = token_info["oid"]
    user = User.query.filter_by(user_oid=oid).first()
    return User.Schema().dump(user)


def getUserProjects(token_info, **kwargs):
    user = _user_by_oid(token_info["oid"])
    projects_of_user = Project.query \
        .join(TeamMember) \
        .filter(TeamMember.user_id == user.user_id) \
        .all()

    return Project.Schema(many=True).dump(projects_of_user)


def getAllProjectStates(**kwargs):
    return State.Schema.fetchAll()


def getUserGuides(token_info, **kwargs):
    user = _user_by_oid(token_info["oid"])
    guides_of_user = Guide.query \
        .join(Request) \
        .filter(Request.requested_by == user.user_id) \
        .all()

    return Guide.Schema(many=True).dump(guides_of_user)


def getUserFAQs(token_info, **kwargs):
    user = _user_by_oid(token_info["oid"])
    guides_of_user = FAQ.query \
        .join(Request) \
        .filter(Request.requested_by == user.user_id) \
        .all()

    return FAQ.Schema(many=True).dump(guides_of_user)


@is_authorized(allowed_roles=[3])
def updateUser(user_id, body, **kwargs):
    return User.Schema.update(user_id, body)


def deleteUser(user_id, token_info, **kwargs):
    if not _is_admin(token_info["oid"]) and not _is_self_user(user_id, token_info["oid"]):
        return _unauthorized()
    return User.Schema.delete(user_id)


def getAllUserRoles(**kwargs):
    return Role.Schema.fetchAll()


@is_authorized(allowed_roles=[3])
def addUserRole(body, **kwargs):
    description = body["description"]
    return Role.Schema.create(Role(description=description))


def getUserRole(userrole_id, **kwargs):
    return Role.Schema.fetch(userrole_id)


@is_authorized(allowed_roles=[3])
def updateUserRole(userrole_id, body, **kwargs):
    return Role.Schema.update(userrole_id, body)


@is_authorized(allowed_roles=[3])
def deleteUserRole(userrole_id, **kwargs):
    return Role.Schema.delete(userrole_id)


@is_authorized(allowed_roles=[2, 3])
def getAllChats(**kwargs):
    return Chat.Schema.fetchAll()


def addPrivateChat(body, **kwargs):
    chat = Chat.Schema.create(Chat(
        name=body["name"],
        description=body["description"]
    ))

    for user_id in body["users"]:
        ChatMember.Schema.create(ChatMember(
            chat_id=chat["chat_id"],
            user_id=user_id
        ))

    return chat


def addGroupChat(body, **kwargs):
    chat = Chat.Schema.create(Chat(
        name=body["name"],
        description=body["description"],
        user_id=body["owner"]
    ))

    return chat


def getChat(chat_id, token_info, **kwargs):
    if not _is_in_chat(chat_id, token_info["oid"]) and not _is_admin(token_info["oid"]):
        return _unauthorized()
    return Chat.Schema.fetch(chat_id)


@is_authorized(allowed_roles=[1, 2, 3])
def updateChat(chat_id, body, token_info, **kwargs):
    if _is_chat_owner_or_admin(chat_id, token_info["oid"]):
        return Chat.Schema.update(chat_id, body)
    return _unauthorized()


@is_authorized(allowed_roles=[1, 2, 3])
def deleteChat(chat_id, token_info, **kwargs):
    if _is_chat_owner_or_admin(chat_id, token_info["oid"]):
        return Chat.Schema.delete(chat_id)
    return _unauthorized()


def getAllChatMessages(chat_id, token_info, **kwargs):
    if not (_is_in_chat(chat_id, token_info["oid"]) or _is_admin(token_info["oid"])):
        return _unauthorized()
    messages = Message.query \
        .filter(Message.chat_id == chat_id) \
        .all()
    return UserMessageSchema(many=True).dump(messages)


def addChatMessage(chat_id, body, token_info, **kwargs):
    if not _is_chat_owner_or_admin_or_in_chat(chat_id, token_info["oid"]):
        return _unauthorized()
    return Message.Schema.create(Message(
        chat_id=chat_id,
        user_id=body["from"],
        content=body["content"],
        created_at=optional("created_at", body)
    ))


def getAllChatUsers(chat_id, token_info, **kwargs):
    if not _is_chat_owner_or_admin_or_in_chat(chat_id, token_info["oid"]):
        return _unauthorized()
    users = ChatMember.query \
        .filter(ChatMember.chat_id == chat_id) \
        .all()
    return ChatMember.Schema(many=True).dump(users)


def addChatUser(chat_id, body, token_info, **kwargs):
    if not _is_chat_owner_or_admin(chat_id, token_info["oid"]):
        return _unauthorized()
    return ChatMember.Schema.create(ChatMember(
        chat_id=chat_id,
        user_id=body["user_id"]
    ))


def deleteChatUser(chat_id, user_id, token_info, **kwargs):
    if not _is_chat_owner_or_admin_or_self(chat_id, user_id, token_info["oid"]):
        return _unauthorized()
    return ChatMember.Schema.delete(chat_id, user_id)


def getUserChats(token_info, **kwargs):
    user = _user_by_oid(token_info["oid"])
    chat_members = ChatMember.query \
        .filter(ChatMember.user_id == user.user_id) \
        .all()
    chats = [Chat.query.filter_by(chat_id=chat_member.chat_id).first() for chat_member in chat_members]
    return Chat.Schema(many=True).dump(chats)


def _unauthorized():
    return {'message': "Unauthorized."}, 403


def _is_moderator(user_oid):
    return _user_by_oid(user_oid).role_id == 2


def _is_admin(user_oid):
    return _user_by_oid(user_oid).role_id == 3


def _is_chat_owner(chat_id, user_oid):
    user = _user_by_oid(user_oid)
    chat = Chat.query.filter_by(chat_id=chat_id).first()
    return chat.user_id == user.user_id


def _is_in_chat(chat_id, user_oid):
    user = _user_by_oid(user_oid)
    return ChatMember.query.filter_by(chat_id=chat_id, user_id=user.user_id).first() is not None


def _user_by_oid(user_oid):
    return User.query.filter_by(user_oid=user_oid).first()


def _is_team_member(project_id, user_oid):
    user = _user_by_oid(user_oid)
    return TeamMember.query \
        .filter_by(project_id=project_id, user_id=user.user_id) \
        .first() is not None


def _is_self_user(user_id, user_oid):
    user = _user_by_oid(user_oid)
    return user.user_id == user_id


def _is_chat_owner_or_admin(chat_id, user_oid):
    return _is_chat_owner(chat_id, user_oid) or _is_admin(user_oid)


def _is_chat_owner_or_admin_or_in_chat(chat_id, user_oid):
    return _is_chat_owner(chat_id, user_oid) or _is_admin(user_oid) or _is_in_chat(chat_id, user_oid)


def _is_chat_owner_or_admin_or_self(chat_id, user_id, user_oid):
    return _is_chat_owner(chat_id, user_oid) or _is_admin(user_oid) or _is_self_user(user_id, user_oid)


def _is_team_member_or_moderator_or_admin(project_id, user_oid):
    return _is_team_member(project_id, user_oid) or _is_moderator(user_oid) or _is_admin(user_oid)
