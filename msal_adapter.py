import msal
from config import config

msal_client = msal.ConfidentialClientApplication(
    config.azure_client.client_id,
    authority=config.azure_client.authority,
    client_credential=config.azure_client.client_secret
)