import startup
from config import config

if __name__ == "__main__":
    startup.connex_app.add_api(config.api.openapi_file, strict_validation=True)
    startup.connex_app.run(
        host=config.server.host,
        port=config.server.port,
    )
