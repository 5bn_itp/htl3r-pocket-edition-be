from flask_cors import CORS
import connexion
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_jwt_extended import JWTManager
from config import config

connex_app = connexion.App(__name__, specification_dir=config.api.specification_dir)
app = connex_app.app
cors = CORS(app, resources={r"/*": {"origins": "*"}})
app.config['SQLALCHEMY_ECHO'] = config.sqlalchemy.echo
app.config['SQLALCHEMY_DATABASE_URI'] = config.sqlalchemy.database_uri
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = config.sqlalchemy.track_modifications
app.config['JWT_TOKEN_LOCATION'] = ['headers']
app.config['JWT_HEADER_NAME'] = ['Authorization']
app.config['JWT_HEADER_TYPE'] = ['Bearer']
db = SQLAlchemy(app)
ma = Marshmallow(app)
jwt = JWTManager(app)