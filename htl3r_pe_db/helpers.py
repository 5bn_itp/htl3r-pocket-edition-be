from datetime import datetime
from htl3r_pe_db.models import *
from connexion import context


def user_exists(user_oid):
    user = User.query.filter_by(user_oid=user_oid).first()
    if user:
        return True
    return False


def optional(key, dict):
    if key in dict:
        return dict[key]
    return None


def add_request_for(token_info):
    user_oid = token_info['oid']
    user = User.query.filter_by(user_oid=user_oid).first()
    request = Request.Schema.create(Request(
        requested_by=user.user_id,
        requested_at=datetime.now(),
        is_accepted=False
    ))
    return request['request_id']


def is_authorized(allowed_roles):
    def decorator(f):
        def wrapper(*args, **kwargs):
            oid = context['token_info']['oid']
            user = User.query.filter_by(user_oid=oid).first()
            role = Role.query.filter_by(role_id=user.role_id).first()

            if not role:
                return {'message': 'Role not found'}, 404

            if role.role_id in allowed_roles:
                return f(*args, **kwargs)

            necessary_roles_description = [r.description for r in allowed_roles_orm]
            unauthorized_msg = {'message': f'Unauthorized. Allowed roles: {str(necessary_roles_description)}'}
            return unauthorized_msg, 403

        return wrapper

    allowed_roles_orm = [Role.query.filter_by(role_id=role_id).first() for role_id in allowed_roles]
    if None in allowed_roles_orm:
        raise Exception('Role not found')

    return decorator

