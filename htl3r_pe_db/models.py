from flask_marshmallow.fields import fields

from startup import db, ma


def add_schema(cls):
    class Schema(ma.SQLAlchemyAutoSchema):
        class Meta:
            include_fk = True
            model = cls
            load_instance = True

        @staticmethod
        def fetch(_id):
            return Schema().dump(cls.query.get(_id))

        @staticmethod
        def fetchAll():
            fetched = cls.query.all()
            return Schema(many=True).dump(fetched)

        @staticmethod
        def create(new):
            db.session.add(new)
            db.session.commit()
            return Schema().dump(new)

        @staticmethod
        def update(_id, updated):
            current = cls.query.get(_id)

            for key, value in updated.items():
                setattr(current, key, value)

            db.session.commit()
            return Schema().dump(current)

        @staticmethod
        def delete(*_id):
            db.session.delete(cls.query.get(_id))
            db.session.commit()
            return 200

    setattr(cls, '__tablename__', cls.__name__)
    cls.Schema = Schema
    return cls


def foreign_key(to: str, nullable=False):
    return db.Column(db.Integer, db.ForeignKey(to), nullable=nullable)


def fk_request():
    return foreign_key(Request.request_id)


@add_schema
class Role(db.Model):
    role_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    description = db.Column(db.VARCHAR(10), nullable=False)


@add_schema
class User(db.Model):
    user_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    user_oid = db.Column(db.Text, nullable=False, unique=True)
    role_id = foreign_key(Role.role_id)
    user_name = db.Column(db.Text, nullable=False)
    user_email = db.Column(db.Text, nullable=False)


@add_schema
class Request(db.Model):
    request_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    requested_by = db.Column(db.Integer, db.ForeignKey(User.user_id), nullable=False)
    requested_at = db.Column(db.DateTime, nullable=False, default=db.func.now())
    is_accepted = db.Column(db.Boolean, nullable=False, default=False)


@add_schema
class FAQ(db.Model):
    faq_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    request_id = fk_request()
    question = db.Column(db.Text, nullable=False)
    answer = db.Column(db.Text, nullable=False)


@add_schema
class Guide(db.Model):
    guide_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    request_id = fk_request()
    topic = db.Column(db.Text, nullable=False)
    content = db.Column(db.Text, nullable=False)


@add_schema
class State(db.Model):
    state_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    description = db.Column(db.VARCHAR(20), nullable=False)


@add_schema
class Project(db.Model):
    project_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    request_id = fk_request()
    state_id = foreign_key(State.state_id)
    title = db.Column(db.VARCHAR(100), nullable=False)
    short_description = db.Column(db.VARCHAR(1000), nullable=False)
    logo = db.Column(db.Text, nullable=True)
    team_image = db.Column(db.Text, nullable=True)
    project_image = db.Column(db.Text, nullable=True)
    long_description = db.Column(db.Text, nullable=False)
    other_information = db.Column(db.Text, nullable=True)


@add_schema
class TeamMember(db.Model):
    project_id = db.Column(db.Integer, db.ForeignKey(Project.project_id), primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey(User.user_id), primary_key=True)
    user = db.relationship(User, backref='team_members', lazy=True)


Project.team = db.relationship(TeamMember, backref='project', lazy=True)


@add_schema
class Chat(db.Model):
    chat_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    user_id = foreign_key(User.user_id, nullable=True)
    name = db.Column(db.VARCHAR(30), nullable=False)
    description = db.Column(db.Text)


@add_schema
class Message(db.Model):
    message_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    user_id = foreign_key(User.user_id, nullable=True)
    user = db.relationship(User, backref='messages')
    chat_id = foreign_key(Chat.chat_id)
    content = db.Column(db.Text, nullable=False)
    created_at = db.Column(db.DateTime, nullable=False, default=db.func.now())


@add_schema
class ChatMember(db.Model):
    chat_id = db.Column(db.Integer, db.ForeignKey(Chat.chat_id), primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey(User.user_id), primary_key=True)


class UserMessageSchema(ma.SQLAlchemyAutoSchema):
    user = fields.Nested(User.Schema)

    class Meta:
        model = Message
        include_fk = True
        load_instance = True


class TeamUserSchema(ma.SQLAlchemyAutoSchema):
    user = fields.Nested(User.Schema)

    class Meta:
        model = TeamMember
        include_fk = True
        load_instance = True


class ProjectTeamSchema(ma.SQLAlchemyAutoSchema):
    team = fields.Nested(TeamUserSchema, many=True)

    class Meta:
        model = Project
        include_fk = True
        load_instance = True
